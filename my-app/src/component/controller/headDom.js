import { tab_link } from '../model/base.js';

export function setStyle()
{
	tab_link.map( obj => 
	{
		
		if(obj.noscript)
		{
			var element1 = document.createElement('noscript');
			var element2 = document.createElement('link');

			document.head.appendChild(element1);
			element2.setAttribute('rel', obj.rel);
			element2.setAttribute('href', obj.href);

			element1.appendChild(element2);
		}
		else if (!obj.noscript)
		{
			element1 = document.createElement('link');
			element1.setAttribute('rel', obj.rel);
			element1.setAttribute('href', obj.href);

			document.head.appendChild(element1);
		}

		return 0;
	}
	);
}
