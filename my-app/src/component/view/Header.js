import React, {Component} from 'react';
import {header_title, header_content, nav_link} from '../model/base.js';

export default class Header extends Component
{
	render()
	{
		return(
			<header id="header">
				<div className="logo">
					<span className="icon fa-diamond"></span>
				</div>
				<div className="content">
					<div className="inner">
						<h1>{header_title}</h1>
						<p>{header_content}</p>
					</div>
				</div>
				<nav>
					<ul>
					{
						nav_link.map(({id, href, content}) => (
							<li key={id} ><a href={href} >{content}</a></li>
						))
					}
					</ul>
				</nav>
			</header>
		);
	}
}
