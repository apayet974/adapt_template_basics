import React, {Component} from 'react';
import {script_link} from '../model/base.js';

export default class Script extends Component
{
	render()
	{
		return(
			<div>	
			{
				script_link.map(({id, src})  => (
					<script key={id} src={src}></script>
				))}
			</div>
		);
	}
}
