import React,{Component} from 'react';
import {intro_title, intro_content, intro_paragraph} from '../model/base.js';

export default class Main extends Component
{
	render()
	{
		return(
			<div id="main">
				<article id= "intro">
					<h2>{intro_title}</h2>

				{
					intro_content.map(({id, image, alternat}) => (
						<span key={id} className="image main"> <img src ={image} alt={alternat} /></span>
					))
				}
				{
					intro_paragraph.map(({id, texte}) => (
						<p key={id}>{texte}</p>
					))
				}
				</article>
			</div>
		);
	}
}
