export const script_link =[
	{id: 1, src: "assets/js/jquery.min.js"},
	{id: 2, src: "assets/js/browser.min.js"},
	{id: 3, src: "assets/breakpoint.min.js"},
	{id: 4, src: "assets/js/util.js"},
	{id: 5, src: "assets/js/main.js"}
];

export const header_title = "DIMENSION";

export const header_content = "Un texte de description du site qui se situe dans le header";

export const nav_link = 
[
	{id: 1, href:"#intro", content: "intro"},
	{id: 2, href:"#element", content: "element"}
]

export const footer_content = 
[
	{id: 1, copyright:"Untitled. 2019 Design by", a_link: "http://wwww.example.com", a_content: "Kap Numerique"}
]

export const intro_title = "Superbus";

export const intro_content =
[
	{id: 1, image:"images/pic01.jpg", alternat:"pic"}
]

export const intro_paragraph =
[
	{id: 1 , texte:"Allô Lola c'est encore moi"},
	{id: 2, texte:"J'ai beaucoup pensé à toi Lola"}
]

export const tab_link =
[
	{id: 1, rel: "stylesheet", href: "assets/css/main.css", noscript: false},
	{id: 2, rel: "stylesheet", href: "assets/css/noscript.css", noscript: true}
]
